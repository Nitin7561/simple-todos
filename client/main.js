import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import '../imports/ui/body.js';
import '../imports/api/tasks.js';
import AddEvent from '../imports/react/AddEvent.js'
import DisplayTask from '../imports/react/DisplayTask.js'


import './main.html';


Template.addEvent.helpers({
    AddEvent(){
        return AddEvent
    }
})

Template.displayTask.helpers({
    DisplayTask(){
        return DisplayTask
    }
})


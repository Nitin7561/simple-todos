import React, { useEffect } from 'react'
import { Card, CardHeader, CardBody, CardFooter, ImageHeader } from 'react-simple-card';
import { Project } from '../api/projects.js';
import { Tasks } from '../api/tasks.js';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';
import './displayTask.css'

export default (props) => {
    const [allProjects, setProject] = React.useState([]);
    updateTask = (projectId, taskName, projectName, checked) => {
     
        Tasks._collection.update({ text: taskName, projectName }, { $set: { checked: checked ? false : true } })
        Project._collection.update({ "task.text": taskName, _id: projectId }, { $set: { "task.$.checked": checked ? false : true, updatedAt: new Date() } })
        Tasks._collection.findOne({ text: taskName, projectName })

    }

    deleteTask = (projectId, taskName, projectName) => {
        event.preventDefault();
        Project._collection.update({ "task.text": taskName, _id: projectId }, { $pull: { task: { text: taskName } } })
        Tasks._collection.remove({ text: taskName, projectName });

    }


    useEffect(() => {
        this.listsTracker = Tracker.autorun(() => {
            Meteor.subscribe('projects');
            res = Project.find({}).fetch();
            setProject(res);
            // console.log(res)
        });
    }, [])

    return (
        <div className="main">
            {allProjects.map((project, index) => {
                // { console.log("whatsup",project) }          
                return (<Card className="card">
                    <ImageHeader imageSrc={"http://localhost:3000/images/" + index + ".jpg"} />
                    <CardBody className="cardBody">
                        <b>{project.projectName}</b>
                    </CardBody>
                    <CardFooter className="cardFooter">

                        <ul style={{ "listStyleType": "none" }}>
                            {project.task != undefined && project.task.length > 0 ?
                                project.task.map((item, index) => {
                                    var temp = project.task[project.task.length - index - 1]
                                    return (
                                        <div>
                                            <li style={{ "textDecoration": temp.checked ? "line-through" : "", "width": "100%", "padding": "8px 0px 8px 0px" }} key={temp.text}>
                                                <input id={temp.text + project.projectName} type="checkbox" checked={temp.checked} onChange={() => updateTask(project._id, temp.text, project.projectName, temp.checked)} />
                                                {temp.text} <button onClick={() => deleteTask(project._id, temp.text, project.projectName)} >❌</button> </li>
                                        </div>
                                    )
                                }) : "EMPTY LIST"
                            }
                        </ul>

                    </CardFooter>
                </Card>)
            })}
        </div>

    );
};
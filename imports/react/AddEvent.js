import React, { useEffect } from 'react'
import { Project } from '../api/projects.js';
import DisplayTask from '../react/DisplayTask'
import './displayTask.css'


export default () => {
  const [name, setName] = React.useState("");


  handleSubmit = () => {
    event.preventDefault();
    
    if (Project.find({projectName:name }).fetch().length) {
      alert("Project name already exists")
    }
    else {
      Project._collection.insert({ projectName:name ,updatedAt:new Date()})
      setName("")
    }
    // console.log("Array",Project.find({}).fetch())

  }
  return (
    <form onSubmit={this.handleSubmit}>
      <label className="label" >
        Add new Project :
      </label>
        <input className="input-project" type="text" name="name" value={name} onChange={e => setName(e.target.value)} required />
      <input type="submit" value="Add" />
    </form>


  );
};
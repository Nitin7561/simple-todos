import { Template } from 'meteor/templating';
import './task.js';
import './body.html';

import { Tasks } from '../api/tasks.js';
import { Project } from '../api/projects.js';
 
Template.body.helpers({
  tasks() {
    return Tasks._collection.find({}, { sort: { createdAt: -1 } });
  },
});


Template.body.events({
  'submit .new-task'(event) {
  
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const text = target.text.value;
    const projectName=document.getElementById("projectName").options[document.getElementById("projectName").selectedIndex].value
    // console.log(projectName)
    if(projectName=="none" || text==""){
      let message=text==""?"Todo can't be an empty string":"Please Select an existing project or create new"
      alert(message)
      return 
    }
   
    // Insert a task into the collection
    Tasks._collection.insert({
      text,
      projectName,
      createdAt: new Date(), // current time
    });

    var doc = Project.findOne({projectName})
    console.log("doc",doc)
    var res=Project._collection.update({_id: doc._id},{ $push: { task: {text,checked:false} },$set:{updatedAt:new Date()} })
    console.log(res)
    
   

    // Clear form
    target.text.value = '';
  },
});
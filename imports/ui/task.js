import { Template } from 'meteor/templating';
 
import { Tasks } from '../api/tasks.js';
import {Project} from '../api/projects.js'
 
import './task.html';
 
Template.task.events({
  'click .toggle-checked'() {
    // Set the checked property to the opposite of its current value
    Tasks._collection.update(this._id, {
      $set: { checked: ! this.checked },
    });
    var res=Tasks._collection.find(this._id).fetch()
    Project._collection.update({"task.text":res[0].text,projectName:res[0].projectName},{ $set: { "task.$.checked":! this.checked,updatedAt:new Date() }})
    var query=Project._collection.find({"task.text":res[0].text}).fetch()

    console.log("----------------------",query)


    
  },
  'click .delete'() {
    var res=Tasks._collection.find(this._id).fetch()
    Project._collection.update({"task.text":res[0].text,projectName:res[0].projectName},{ $pull: {task:{text : res[0].text}}})
    var query=Project._collection.find({"task.text":res[0].text}).fetch()

    console.log("----------------------",query)
    Tasks._collection.remove(this._id);
    


  },
});

Template.projectSelect.events({
  'change .event-checked'(e){
    console.log("nitin",e.currentTarget.value)
   
  }
})

Template.projectSelect.helpers({
  'projects': function(){
      return Project.find().fetch();
  }
});
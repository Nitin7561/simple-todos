import { Mongo } from 'meteor/mongo';
 
export const Project = new Mongo.Collection('projects');

if (Meteor.isServer) {
    Meteor.publish('projects', function () {
      return Project.find({ userId: this.userId });
    });
  }